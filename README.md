# Pac-Man AI 

## Membros
- Eduardo Meneghim Alves Silva
- Gustavo Lofrese Carvalho
- Leonardo de Souza Fiamoncini

## Links úteis
- [ ] [Link do projeto](https://inst.eecs.berkeley.edu/~cs188/sp24/projects/proj2/)
- [ ] [Link da documentação](https://docs.google.com/document/d/10CVUiAwXq1GZw1QTvDwrfSEwfgaBEUzlb860GLZiG8Y)
- [ ] [Link abnTeX2](https://github.com/abntex/abntex2/wiki/Download)

## Comandos
- ReflexAgent
  - `python3 pacman.py -p ReflexAgent -l smallClassic`
  - `python3 pacman.py -p ReflexAgent -l minimaxClassic`
  - `python3 pacman.py -p ReflexAgent -l testClassic`
  - `python3 pacman.py -p ReflexAgent -l openClassic`
  - `python3 pacman.py -p ReflexAgent -l mediumClassic`
  - `python3 pacman.py -p ReflexAgent -l trappedClassic -q -n 5`
  
- MiniMax
  - `python3 pacman.py -p MinimaxAgent -a depth=3 -l smallClassic`
  - `python3 pacman.py -p MinimaxAgent -a depth=5 -l smallClassic`
  - `python3 pacman.py -p MinimaxAgent -a depth=3 -l minimaxClassic`
  - `python3 pacman.py -p MinimaxAgent -a depth=4 -l minimaxClassic`
  - `python3 pacman.py -p MinimaxAgent -a depth=3 -l openClassic`
  - `python3 pacman.py -p MinimaxAgent -a depth=3 -l testClassic`
  - `python3 pacman.py -p MinimaxAgent -a depth=3 -l trappedClassic`

- AlphaBeta
  - `python3 pacman.py -p AlphaBetaAgent -a depth=3 -l smallClassic`
  - `python3 pacman.py -p AlphaBetaAgent -a depth=5 -l smallClassic`
  - `python3 pacman.py -p AlphaBetaAgent -a depth=3 -l minimaxClassic`
  - `python3 pacman.py -p AlphaBetaAgent -a depth=5 -l minimaxClassic`
  - `python3 pacman.py -p AlphaBetaAgent -a depth=3 -l openClassic`
  - `python3 pacman.py -p AlphaBetaAgent -a depth=3 -l testClassic`
  - `python3 pacman.py -p AlphaBetaAgent -l trappedClassic -a depth=3 -q -n 5`

- Expectimax
  - `python3 pacman.py -p ExpectimaxAgent -a depth=3 -l smallClassic`
  - `python3 pacman.py -p ExpectimaxAgent -a depth=5 -l smallClassic`
  - `python3 pacman.py -p ExpectimaxAgent -a depth=3 -l minimaxClassic`
  - `python3 pacman.py -p ExpectimaxAgent -a depth=5 -l minimaxClassic`
  - `python3 pacman.py -p ExpectimaxAgent -a depth=3 -l openClassic`
  - `python3 pacman.py -p ExpectimaxAgent -a depth=3 -l testClassic`
  - `python3 pacman.py -p ExpectimaxAgent -l trappedClassic -a depth=3 -q -n 5`
