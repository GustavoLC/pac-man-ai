# multiAgents.py
# --------------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).
from pyparsing import actions
from operator import itemgetter

from util import manhattanDistance
from array import *
from game import Directions
import random, util

from game import Agent
from pacman import GameState

from collections import namedtuple

State = namedtuple("State", "action score")


class ReflexAgent(Agent):
    """
    A reflex agent chooses an action at each choice point by examining
    its. alternatives via a state evaluation function

    The code below is provided as a guide.  You are welcome to change
    it in any way you see fit, so long as you don't touch our method
    headers.
    """

    def getAction(self, gameState: GameState):
        """
        You do not need to change this method, but you're welcome to.

        getAction chooses among the best options according to the evaluation function.

        Just like in the previous project, getAction takes a GameState and returns
        some Directions.X for some X in the set {NORTH, SOUTH, WEST, EAST, STOP}
        """
        # Collect legal moves and successor states
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best

        "Add more of your code here if you want to"

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState: GameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current and proposed successor
        GameStates (pacman.py) and returns a number, where higher numbers are better.

        The code below extracts some useful information from the state, like the
        remaining food (newFood) and Pacman position after moving (newPos).
        newScaredTimes holds the number of moves that each ghost will remain
        scared because of Pacman having eaten a power pellet.

        Print out these variables to see what you're getting, then combine them
        to create a masterful evaluation function.
        """

        successorGameState = currentGameState.generatePacmanSuccessor(action)
        newPos = successorGameState.getPacmanPosition()
        newFood = successorGameState.getFood()
        newGhostStates = successorGameState.getGhostStates()
        newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

        customScore = 0

        ghostPacmanDistance = [manhattanDistance(newPos, ghostPosition.configuration.getPosition()) for ghostPosition in
                               newGhostStates]

        foodDistances = [manhattanDistance(newPos, foodPosition) for foodPosition in newFood.asList()]

        if len(foodDistances) == 0:
            return successorGameState.getScore()

        closestFood = min(foodDistances)
        closestGhost = min(ghostPacmanDistance)

        # Se o tempo para cada fantasma permanecer assustado for maior que zero e menor ou igual
        # à distância da posição do pacman para o fantasma mais próximo aumenta o score

        if any(newScaredTimes):
            customScore = 0

            for ghostIndex, scaredTime in enumerate(newScaredTimes):
                if scaredTime > 0:
                    closestGhost = ghostPacmanDistance[ghostIndex]

                    if scaredTime > closestGhost:
                        customScore += 10
                    else:
                        customScore -= 10

        if action == 'Stop':
            customScore -= 50

        return successorGameState.getScore() + customScore + (closestGhost / (closestFood * 10))


def scoreEvaluationFunction(currentGameState: GameState):
    """
    This default evaluation function just returns the score of the state.
    The score is the same one displayed in the Pacman GUI.

    This evaluation function is meant for use with adversarial search agents
    (not reflex agents).
    """
    return currentGameState.getScore()


class MultiAgentSearchAgent(Agent):
    """
    This class provides some common elements to all of your
    multi-agent searchers.  Any methods defined here will be available
    to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

    You *do not* need to make any changes here, but you can if you want to
    add functionality to all your adversarial search agents.  Please do not
    remove anything, however.

    Note: this is an abstract class: one that should not be instantiated.  It's
    only partially specified, and designed to be extended.  Agent (game.py)
    is another abstract class.
    """

    def __init__(self, evalFn='scoreEvaluationFunction', depth='2'):
        self.index = 0  # Pacman is always agent index 0
        self.evaluationFunction = util.lookup(evalFn, globals())
        self.depth = int(depth)


class MinimaxAgent(MultiAgentSearchAgent):

    def getAction(self, gameState: GameState):
        return self.minimax(gameState, self.index, self.depth, True).action

    # Seleciono uma ação aleatória das ações com o mesmo score desejado
    def randomAction(self, actions: list, selectedScore: int, scores: list) -> Directions:
        selectedIndex = [index for index, score in enumerate(scores) if score == selectedScore]
        return actions[random.choice(selectedIndex)]

    def minimax(self, gameState: GameState, agentIndex: int, depth: int, maximizing: bool) -> State:

        # Caso o jogo acabe, o pacman deve permanecer parado e a sua pontuação é a posição atual
        if depth == 0 or gameState.isWin() or gameState.isLose():
            return State(Directions.STOP, self.evaluationFunction(gameState))

        # Verifico as jogadas possíveis
        actions = gameState.getLegalActions(agentIndex)
        scores = []

        # Verificamos se é o último fantasma
        isLastGhost = agentIndex == gameState.getNumAgents() - 1

        if isLastGhost:
            newDepth = depth - 1
            # Se o último agente a jogar foi um Fantasma,o próximo é o Pacman (Sempre indíce 0)
            newAgentIndex = 0
        else:
            newAgentIndex = agentIndex + 1
            newDepth = depth

        for action in actions:
            newGameState = gameState.generateSuccessor(agentIndex, action)
            # Faço novamente o minimax com as novas informações, passando False no parametro "Maximizing" caso seja maximizing,
            # e a variável isLastGhost caso não seja, pois caso seja o último fantasma, alternamos para maximizing
            newState = self.minimax(newGameState, newAgentIndex, newDepth, False if maximizing else isLastGhost)
            scores.append(newState.score)

        # Pegamos o máximo de score caso seja maximizing, e o mínimo caso contrário
        selectedScore = max(scores) if maximizing else min(scores)

        # Pegamos uma ação aleatória das ações possíveis com o mesmo score desejado
        chosenAction = self.randomAction(actions, selectedScore, scores)
        return State(chosenAction, selectedScore)


class AlphaBetaAgent(MultiAgentSearchAgent):

    def getAction(self, gameState: GameState):
        return self.minimax(gameState, self.index, self.depth).action

    def minimax(self, gameState: GameState, agentIndex: int, depth: int, alpha=-999999, beta=999999, ) -> State:

        # Caso o jogo acabe, o pacman deve permanecer parado e a sua pontuação é a posição atual
        if depth == 0 or gameState.isWin() or gameState.isLose():
            return State(Directions.STOP, self.evaluationFunction(gameState))

        # Verificamos se é o último fantasma, se for o próximo será o PacMan
        agentsNum = gameState.getNumAgents()
        agentIndex %= agentsNum
        if agentIndex == agentsNum - 1:
            depth -= 1

        # Pacman
        if agentIndex == 0:
            actions = []
            for action in gameState.getLegalActions(agentIndex):
                newScore = self.minimax(gameState.generateSuccessor(agentIndex, action), agentIndex + 1, depth, alpha,
                                        beta).score
                actions.append(State(action, newScore))

                if newScore > beta:
                    return State(action, newScore)
                alpha = max(alpha, newScore)
            # Faz a avaliação de qual o maior Score (itemgetter(1) == State.score)
            return max(actions, key=itemgetter(1))
        # Fantasmas
        else:
            actions = []
            for action in gameState.getLegalActions(agentIndex):
                newScore = self.minimax(gameState.generateSuccessor(agentIndex, action), agentIndex + 1, depth, alpha,
                                        beta).score
                actions.append(State(action, newScore))

                if newScore < alpha:
                    return State(action, newScore)
                beta = min(beta, newScore)
            # Faz a avaliação de qual o menor Score (itemgetter(1) == State.score)
            return min(actions, key=itemgetter(1))


class ExpectimaxAgent(MultiAgentSearchAgent):

    def getAction(self, gameState: GameState):
        return self.expectimax(gameState, self.index, self.depth).action

    def expectimax(self, gameState: GameState, agentIndex: int, depth: int) -> State:
        # Caso o jogo acabe, o pacman deve permanecer parado e a sua pontuação é a posição atual
        if depth == 0 or gameState.isWin() or gameState.isLose():
            return State(Directions.STOP, self.evaluationFunction(gameState))

        # Verificamos se é o último fantasma
        agentsNum = gameState.getNumAgents()
        agentIndex %= agentsNum
        #Caso seja, o próximo é o pacman (diminuimos a profundidade em 1)
        if agentIndex == agentsNum - 1:
            depth -= 1

        # Pacman
        if agentIndex == 0:
            finalScore, finalAction = -999999, None
            # Para cada ação, conseguimos um score com a mesma função expectimax
            for action in gameState.getLegalActions(agentIndex):
                newScore = self.expectimax(gameState.generateSuccessor(agentIndex, action), agentIndex + 1, depth).score
                #Caso seja considerado melhor, selecionamos essa jogada
                if newScore > finalScore:
                    finalScore, finalAction = newScore, action
            return State(finalAction, finalScore)

        # Fantasmas
        else:
            score = 0
            actions = gameState.getLegalActions(agentIndex)
            for action in actions:
                #Somamos o score para decidir qual a melhor sequência de jogadas
                score += self.expectimax(gameState.generateSuccessor(agentIndex, action), agentIndex + 1, depth).score
            return State(Directions.STOP, score / len(actions))

def betterEvaluationFunction(currentGameState: GameState):
    # Pontuação base do estado atual
    score = currentGameState.getScore()

    # Posição atual do Pacman
    newPos = currentGameState.getPacmanPosition()
    
    # Obter a comida e os estados dos fantasmas
    newFood = currentGameState.getFood()
    newGhostStates = currentGameState.getGhostStates()
    newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]

    # Distância para todos os fantasmas e para a comida
    ghostDistances = [manhattanDistance(newPos, ghost.getPosition()) for ghost in newGhostStates]
    foodDistances = [manhattanDistance(newPos, foodPos) for foodPos in newFood.asList()]

    # Distância mínima para comida e para o fantasma mais próximo
    if foodDistances:
        closestFood = min(foodDistances)
    else:
        closestFood = 0  # Se não há comida, usamos 0

    if ghostDistances:
        closestGhost = min(ghostDistances)
    else:
        closestGhost = float('inf')  # Se não há fantasmas, usamos infinito

    # Avaliação da comida
    foodScore = 0
    if closestFood > 0:
        foodScore = 10 / closestFood  # Quanto mais próxima a comida, mais pontos são adicionados

    # Avaliação dos fantasmas
    ghostScore = 0
    for i, ghost in enumerate(newGhostStates):
        distance = ghostDistances[i]
        if distance > 0:
            if newScaredTimes[i] > 0:
                # Se o fantasma está assustado, adicione pontos baseados na distância
                ghostScore += 100 / distance
            else:
                # Se o fantasma não está assustado, penalize baseado na distância
                ghostScore -= 10 / distance
        else:
            # Se Pacman está na mesma posição que um fantasma não assustado, é um caso de derrota
            return -999999

    # Considera a segurança geral com base no tempo de assombração dos fantasmas
    scaredGhostFactor = 0
    if any(newScaredTimes):
        scaredGhostFactor = sum(newScaredTimes) / len(newScaredTimes)  # Média do tempo assustado

    # Calcula a pontuação final combinando todos os fatores
    finalScore = score + foodScore + ghostScore + scaredGhostFactor

    return finalScore


# Abbreviation
better = betterEvaluationFunction
